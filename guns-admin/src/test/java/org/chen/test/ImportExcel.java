package org.chen.test;

import com.github.crab2died.ExcelUtils;
import java.io.IOException;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class ImportExcel {
  // JDBC 驱动名及数据库 URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://localhost:3306/guns";

  // 数据库的用户名与密码，需要根据自己的设置
  static final String USER = "root";
  static final String PASS = "root";

  public static void main(String[] args) throws IOException, InvalidFormatException {
    JdbcTemplate jdbcTemplate = new JdbcTemplate(new SingleConnectionDataSource(
        DB_URL, USER, PASS, false
    ));
    String path = "D:\\我的坚果云\\文件同步\\沁和\\报价.xls";

    List<List<String>> lists = ExcelUtils.getInstance().readExcel2List(
        path,
        2,
        Integer.MAX_VALUE,
        0);
    System.out.println("读取Excel至String数组：");
    for (List<String> list : lists) {
      System.out.println(list);
    }
  }
}
