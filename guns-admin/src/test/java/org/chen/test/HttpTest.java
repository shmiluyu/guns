package org.chen.test;

import static jodd.jerry.Jerry.jerry;

import com.alibaba.fastjson.JSON;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import jodd.http.Cookie;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.ProxyInfo;
import jodd.http.net.SocketHttpConnectionProvider;
import jodd.io.FileUtil;
import jodd.jerry.Jerry;

public class HttpTest {

  public static void main(String[] args) throws IOException {
    //downloadZm();
    SocketHttpConnectionProvider s = new SocketHttpConnectionProvider();
    s.useProxy(ProxyInfo.socks5Proxy("127.0.0.1", 1080, null, null));

    HttpRequest httpRequest = HttpRequest.get("https://www.youtube.com")
        .withConnectionProvider(s);
    HttpResponse response = httpRequest.send();
    System.out.println(response.body());

  }
  public static void main3(String[] args) {
    //https://3dwarehouse.sketchup.com/warehouse/v1.0/entities/ua921bcd6-ef2d-446f-b340-1c1098ed7de8/binaries/s14?download=true
    HttpRequest httpRequest = HttpRequest.get("https://3dwarehouse.sketchup.com/warehouse/v1.0/entities/ua921bcd6-ef2d-446f-b340-1c1098ed7de8/binaries/s14?download=true");
    httpRequest.cookies(
        new Cookie("TOS_ACCEPTED_VERSION","2017-03-01")
    );
    HttpResponse response = httpRequest.send();
    System.out.println(response.header("location"));
  }

  public static void main2(String[] args) {
    //https://3dwarehouse.sketchup.com/model/ua921bcd6-ef2d-446f-b340-1c1098ed7de8/Apple-15-Macbook-Pro-Retina-2013
    HttpRequest httpRequest = HttpRequest.get("https://3dwarehouse.sketchup.com/model/ua921bcd6-ef2d-446f-b340-1c1098ed7de8/Apple-15-Macbook-Pro-Retina-2013")
        .acceptEncoding("gzip");
    HttpResponse response = httpRequest.send();
    Jerry jerry = jerry(response.unzip().bodyText());
    System.out.println(jerry.$("div.model-preview img").first().attr("src"));
  }

  public static void downloadZm() throws IOException {
    //https://download.znzmo.com/2f76f56ea88cc720345cf79abe78a677e5a3e809.7z?Expires=1537609739&OSSAccessKeyId=LTAIKZTSmAKbt5X0&Signature=%2BqVzEDfggC1NY7htJNVVk3ZIkrE%3D
    HttpRequest request = HttpRequest.get(
        "http://su.znzmo.com/commodityDetail/getDownLoadCommodityUrl.do?skuId=26686410")
        .acceptJson();
    request.header("Cookie","JSESSIONID=8B9067DB758FE870940BAE1DA8FF1214;");
    HttpResponse response = request.send();
    Map map = JSON.parseObject(response.bodyText(), Map.class);
    String downloadURL = map.get("data").toString();

    response = HttpRequest.get(downloadURL).send();
    byte[] bytes = response.bodyBytes();
    if(response.mediaType().indexOf("7z-compressed")>0){
      FileUtil.writeBytes(new File("d://test.7z"), bytes);
    }else{
      if(response.bodyText().startsWith("Rar")){
        FileUtil.writeBytes(new File("d://test.rar"), bytes);
      }else{
        FileUtil.writeBytes(new File("d://test.skp"), bytes);
      }
    }

  }
}
