package org.chen.test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import jodd.io.FileUtil;
import net.sf.sevenzipjbinding.ExtractOperationResult;
import net.sf.sevenzipjbinding.IInArchive;
import net.sf.sevenzipjbinding.SevenZip;
import net.sf.sevenzipjbinding.SevenZipException;
import net.sf.sevenzipjbinding.SevenZipNativeInitializationException;
import net.sf.sevenzipjbinding.impl.RandomAccessFileInStream;
import net.sf.sevenzipjbinding.simple.ISimpleInArchive;
import net.sf.sevenzipjbinding.simple.ISimpleInArchiveItem;

public class SevenZipTest {

  public static void main(String[] args) {
    try {
      SevenZip.initSevenZipFromPlatformJAR();
      System.out.println("7-Zip-JBinding library was initialized");
      extractFirstSkpFile(new File("/Users/shmiluyu/conf/jodd-4485635540757932883.7z"),
                          new File("/Users/shmiluyu/conf/test.skp"));
    } catch (SevenZipNativeInitializationException e) {
      e.printStackTrace();
    }
  }

  private static boolean extractFirstSkpFile(File zipFile, File dest) {
    RandomAccessFile randomAccessFile = null;
    IInArchive inArchive = null;
    try {
      randomAccessFile = new RandomAccessFile(zipFile, "r");
      inArchive = SevenZip.openInArchive(null,
                                         new RandomAccessFileInStream(randomAccessFile));
      ISimpleInArchive simpleInArchive = inArchive.getSimpleInterface();

      for (ISimpleInArchiveItem item : simpleInArchive.getArchiveItems()) {
        if (!item.isFolder() && !item.isEncrypted()
            && item.getPath().endsWith("skp")) {
          ExtractOperationResult result;
          final boolean[] append = {false};
          result = item.extractSlow(data -> {
            try {
              if (append[0]) {
                FileUtil.appendBytes(dest, data);
              } else {
                FileUtil.writeBytes(dest, data);
                append[0] = true;
              }
            } catch (IOException e) {
              e.printStackTrace();
            }
            return data.length;
          });
          if (result != ExtractOperationResult.OK) {
            return false;
          }
          break;
        }
      }
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println("Error occurs: " + e);
      return false;
    } finally {
      if (inArchive != null) {
        try {
          inArchive.close();
        } catch (SevenZipException e) {
          System.err.println("Error closing archive: " + e);
        }
      }
      if (randomAccessFile != null) {
        try {
          randomAccessFile.close();
        } catch (IOException e) {
          System.err.println("Error closing file: " + e);
        }
      }
    }
  }
}
