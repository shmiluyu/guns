/**
 * 材质分类管理初始化
 */
var SuSkmCategory = {
    id: "SuSkmCategoryTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SuSkmCategory.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'pid', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SuSkmCategory.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SuSkmCategory.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加材质分类
 */
SuSkmCategory.openAddSuSkmCategory = function () {
    var index = layer.open({
        type: 2,
        title: '添加材质分类',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/suSkmCategory/suSkmCategory_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看材质分类详情
 */
SuSkmCategory.openSuSkmCategoryDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '材质分类详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/suSkmCategory/suSkmCategory_update/' + SuSkmCategory.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除材质分类
 */
SuSkmCategory.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/suSkmCategory/delete", function (data) {
            Feng.success("删除成功!");
            SuSkmCategory.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("suSkmCategoryId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询材质分类列表
 */
SuSkmCategory.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SuSkmCategory.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SuSkmCategory.initColumn();
    var table = new BSTable(SuSkmCategory.id, "/suSkmCategory/list", defaultColunms);
    table.setPaginationType("client");
    SuSkmCategory.table = table.init();
});
