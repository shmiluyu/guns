/**
 * 初始化材质分类详情对话框
 */
var SuSkmCategoryInfoDlg = {
    suSkmCategoryInfoData : {}
};

/**
 * 清除数据
 */
SuSkmCategoryInfoDlg.clearData = function() {
    this.suSkmCategoryInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SuSkmCategoryInfoDlg.set = function(key, val) {
    this.suSkmCategoryInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SuSkmCategoryInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SuSkmCategoryInfoDlg.close = function() {
    parent.layer.close(window.parent.SuSkmCategory.layerIndex);
}

/**
 * 收集数据
 */
SuSkmCategoryInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('pid');
}

/**
 * 提交添加
 */
SuSkmCategoryInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/suSkmCategory/add", function(data){
        Feng.success("添加成功!");
        window.parent.SuSkmCategory.table.refresh();
        SuSkmCategoryInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.suSkmCategoryInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SuSkmCategoryInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/suSkmCategory/update", function(data){
        Feng.success("修改成功!");
        window.parent.SuSkmCategory.table.refresh();
        SuSkmCategoryInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.suSkmCategoryInfoData);
    ajax.start();
}

$(function() {

});
