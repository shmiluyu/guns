/**
 * 报价模板管理初始化
 */
var Quote = {
    id: "QuoteTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Quote.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '', field: 'code', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'unit', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'price', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'pinyin', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'remark', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Quote.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Quote.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加报价模板
 */
Quote.openAddQuote = function () {
    var index = layer.open({
        type: 2,
        title: '添加报价模板',
        area: ['800px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/quote/quote_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看报价模板详情
 */
Quote.openQuoteDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '报价模板详情',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/quote/quote_update/' + Quote.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除报价模板
 */
Quote.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/quote/delete", function (data) {
            Feng.success("删除成功!");
            Quote.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("quoteId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询报价模板列表
 */
Quote.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Quote.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Quote.initColumn();
    var table = new BSTable(Quote.id, "/quote/list", defaultColunms);
    table.setPaginationType("client");
    Quote.table = table.init();
});
