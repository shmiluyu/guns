/**
 * 初始化报价模板详情对话框
 */
var QuoteInfoDlg = {
    quoteInfoData : {}
};

/**
 * 清除数据
 */
QuoteInfoDlg.clearData = function() {
    this.quoteInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
QuoteInfoDlg.set = function(key, val) {
    this.quoteInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
QuoteInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
QuoteInfoDlg.close = function() {
    parent.layer.close(window.parent.Quote.layerIndex);
}

/**
 * 收集数据
 */
QuoteInfoDlg.collectData = function() {
    this
    .set('id')
    .set('code')
    .set('title')
    .set('unit')
    .set('price')
    .set('pinyin')
    .set('remark');
}

/**
 * 提交添加
 */
QuoteInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/quote/add", function(data){
        Feng.success("添加成功!");
        window.parent.Quote.table.refresh();
        QuoteInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.quoteInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
QuoteInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/quote/update", function(data){
        Feng.success("修改成功!");
        window.parent.Quote.table.refresh();
        QuoteInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.quoteInfoData);
    ajax.start();
}

$(function() {

});
