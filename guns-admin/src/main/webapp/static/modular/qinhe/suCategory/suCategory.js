/**
 * 模型分类管理初始化
 */
var SuCategory = {
  id: "SuCategoryTable",	//表格id
  seItem: null,		//选中的条目
  table: null,
  layerIndex: -1
};

/**
 * 初始化表格的列
 */
SuCategory.initColumn = function () {
  return [
    {field: 'selectItem', radio: true},
    {
      title: 'ID',
      field: 'id',
      visible: true,
      align: 'center',
      valign: 'middle',
      width: '50px'
    },
    {
      title: '类名',
      field: 'name',
      visible: true,
      align: 'center',
      valign: 'middle'
    }
  ];
};

/**
 * 检查是否选中
 */
SuCategory.check = function () {
  var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
  if (selected.length == 0) {
    Feng.info("请先选中表格中的某一记录！");
    return false;
  } else {
    SuCategory.seItem = selected[0];
    return true;
  }
};

/**
 * 点击添加模型分类
 */
SuCategory.openAddSuCategory = function () {
  var index = layer.open({
    type: 2,
    title: '添加模型分类',
    area: ['800px', '420px'], //宽高
    fix: false, //不固定
    maxmin: true,
    content: Feng.ctxPath + '/suCategory/suCategory_add'
  });
  this.layerIndex = index;
};

/**
 * 打开查看模型分类详情
 */
SuCategory.openSuCategoryDetail = function () {
  if (this.check()) {
    var index = layer.open({
      type: 2,
      title: '模型分类详情',
      area: ['800px', '420px'], //宽高
      fix: false, //不固定
      maxmin: true,
      content: Feng.ctxPath + '/suCategory/suCategory_update/'
      + SuCategory.seItem.id
    });
    this.layerIndex = index;
  }
};

/**
 * 删除模型分类
 */
SuCategory.delete = function () {
  if (SuCategory.check()) {
    var ajax = new $ax(Feng.ctxPath + "/suCategory/delete?suCategoryId="+SuCategory.seItem.id, function (data) {
      Feng.success("删除成功!");
      SuCategory.table.refresh();
    }, function (data) {
      Feng.error("删除失败!" + data.responseJSON.message + "!");
    });
    ajax.start();
  }
};

/**
 * 查询模型分类列表
 */
SuCategory.search = function () {
  var queryData = {};
  queryData['condition'] = $("#condition").val();
  SuCategory.table.refresh({query: queryData});
};

$(function () {
  var defaultColunms = SuCategory.initColumn();
  var table = new BSTreeTable(SuCategory.id, "/suCategory/list",
      defaultColunms);
  table.setExpandColumn(2);
  table.setIdField("id");
  table.setCodeField("id");
  table.setParentCodeField("pid");
  table.setExpandAll(true);
  SuCategory.table = table.init();
});
