/**
 * 初始化模型分类详情对话框
 */
var SuCategoryInfoDlg = {
  suCategoryInfoData: {}
};

/**
 * 清除数据
 */
SuCategoryInfoDlg.clearData = function () {
  this.suCategoryInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SuCategoryInfoDlg.set = function (key, val) {
  this.suCategoryInfoData[key] = (typeof val == "undefined") ? $("#"
      + key).val() : val;
  return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SuCategoryInfoDlg.get = function (key) {
  return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SuCategoryInfoDlg.close = function () {
  parent.layer.close(window.parent.SuCategory.layerIndex);
}

/**
 * 收集数据
 */
SuCategoryInfoDlg.collectData = function () {
  this
  .set('id')
  .set('name')
  .set('pid');
}

SuCategoryInfoDlg.onClickPCat = function(e, treeId, treeNode) {
  $("#pName").attr("value", SuCategoryInfoDlg.zTreeInstance.getSelectedVal());
  $("#pid").attr("value", treeNode.id);
}


/**
 * 提交添加
 */
SuCategoryInfoDlg.addSubmit = function () {

  this.clearData();
  this.collectData();

  //提交信息
  var ajax = new $ax(Feng.ctxPath + "/suCategory/add", function (data) {
    Feng.success("添加成功!");
    window.parent.SuCategory.table.refresh();
    SuCategoryInfoDlg.close();
  }, function (data) {
    Feng.error("添加失败!" + data.responseJSON.message + "!");
  });
  ajax.set(this.suCategoryInfoData);
  ajax.start();
}

/**
 * 提交修改
 */
SuCategoryInfoDlg.editSubmit = function () {

  this.clearData();
  this.collectData();

  //提交信息
  var ajax = new $ax(Feng.ctxPath + "/suCategory/update", function (data) {
    Feng.success("修改成功!");
    window.parent.SuCategory.table.refresh();
    SuCategoryInfoDlg.close();
  }, function (data) {
    Feng.error("修改失败!" + data.responseJSON.message + "!");
  });
  ajax.set(this.suCategoryInfoData);
  ajax.start();
}

SuCategoryInfoDlg.showPCatSelectTree = function () {
  var pName = $("#pName");
  var pNameOffset = $("#pName").offset();
  $("#parentCategoryMenu").css({
    left: pNameOffset.left + "px",
    top: pNameOffset.top + pName.outerHeight() + "px"
  }).slideDown("fast");

  $("body").bind("mousedown", onBodyDown);
}

function onBodyDown(event) {
  if (!(event.target.id == "menuBtn" || event.target.id == "parentCategoryMenu"
          || $(
              event.target).parents("#parentCategoryMenu").length > 0)) {
    SuCategoryInfoDlg.hideDeptSelectTree();
  }
}

SuCategoryInfoDlg.hideDeptSelectTree = function () {
  $("#parentCategoryMenu").fadeOut("fast");
  $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

$(function () {
  var ztree = new $ZTree("parentCategoryMenuTree", "/suCategory/tree");
  ztree.bindOnClick(SuCategoryInfoDlg.onClickPCat);
  ztree.init();
  SuCategoryInfoDlg.zTreeInstance = ztree;
});
