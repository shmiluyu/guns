package com.stylefeng.guns.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import javax.xml.bind.DatatypeConverter;

public enum Hash {

    MD5("MD5"),
    SHA1("SHA1"),
    SHA256("SHA-256"),
    SHA512("SHA-512");

    private String name;

    Hash(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public byte[] checksum(File input) {
        try (InputStream in = new FileInputStream(input)) {
            MessageDigest digest = MessageDigest.getInstance(getName());
            byte[] block = new byte[4096];
            int length;
            while ((length = in.read(block)) > 0) {
                digest.update(block, 0, length);
            }
            return digest.digest();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        byte[] checksum = Hash.SHA256.checksum(new File("f:\\亚太明珠负一楼2.skp"));
        //432902481A6223D9152BB06839CAF4563E6251D9F76913969FC3185E29CF572B
        //432902481a6223d9152bb06839caf4563e6251d9f76913969fc3185e29cf572b
        System.out.println(DatatypeConverter.printHexBinary(checksum).toLowerCase());
    }

}