package com.stylefeng.guns.core;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.GunsApplication;
import com.stylefeng.guns.core.util.Hash;
import com.stylefeng.guns.modular.qinhe.service.ISuModelService;
import com.stylefeng.guns.modular.system.model.SuModel;
import java.io.File;
import java.io.IOException;
import javax.xml.bind.DatatypeConverter;
import jodd.http.Cookie;
import jodd.http.HttpRequest;
import jodd.http.ProxyInfo;
import jodd.http.net.SocketHttpConnectionProvider;
import jodd.io.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class Download3DWarehouseTask {

  @Autowired
  ISuModelService suModelService;
  private Logger log = LoggerFactory.getLogger(this.getClass());

  @Async
  public void download(SuModel suModel, String modelid, String thumbpath) throws IOException {
    File jarFolder = new ApplicationHome(GunsApplication.class).getDir();
    long filename = System.currentTimeMillis();
    log.info("start download 3dwarehouse---" + suModel.getModelname());
    //download skp file and move to model folder
    //https://3dwarehouse.sketchup.com/warehouse/v1.0/entities/ua921bcd6-ef2d-446f-b340-1c1098ed7de8/binaries/s14?download=true
    SocketHttpConnectionProvider s = new SocketHttpConnectionProvider();
    s.useProxy(ProxyInfo.socks5Proxy("127.0.0.1", 1080, null, null));
    HttpRequest request = HttpRequest.get(
        "https://3dwarehouse.sketchup.com/warehouse/v1.0/entities/" + modelid
            + "/binaries/s14?download=true")
        .withConnectionProvider(s);
    request.cookies(
        new Cookie("TOS_ACCEPTED_VERSION", "2017-03-01")
    );

    String location = request.send().header("location");
    byte[] bytes = HttpRequest.get(location)
        .withConnectionProvider(s)
        .send()
        .bodyBytes();
    File newFileFolder = new File(
        jarFolder + "/models/");
    newFileFolder.mkdirs();
    File dest = new File(newFileFolder, filename + ".skp");
    FileUtil.writeBytes(dest, bytes);
    suModel.setChecksum(DatatypeConverter.printHexBinary(Hash.SHA256.checksum(dest)).toLowerCase());
    suModel.setFilename(filename + "");
    suModel.setFilesize((long) bytes.length);
    byte[] thumbBytes = HttpRequest.get(thumbpath).withConnectionProvider(s).send().bodyBytes();
    FileUtil.writeBytes(new File(newFileFolder, filename + ".png"), thumbBytes);
    suModelService.insert(suModel);
    log.info("finished 3dwarehouse---" + suModel.getModelname());
  }

  @Async
  public void checksum(File file, String filename) {
    SuModel model = suModelService
        .selectOne(new EntityWrapper<SuModel>().eq("filename", filename));
    model.setChecksum(DatatypeConverter.printHexBinary(Hash.SHA256.checksum(file)).toLowerCase());
    suModelService.updateById(model);
  }
}
