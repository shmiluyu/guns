package com.stylefeng.guns.modular.qinhe.service.impl;

import com.stylefeng.guns.modular.system.model.SuModel;
import com.stylefeng.guns.modular.system.dao.SuModelMapper;
import com.stylefeng.guns.modular.qinhe.service.ISuModelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模型 服务实现类
 * </p>
 *
 * @author shmiuyu
 * @since 2018-09-08
 */
@Service
public class SuModelServiceImpl extends ServiceImpl<SuModelMapper, SuModel> implements ISuModelService {

  @Resource
  private SuModelMapper mapper;

  @Override
  public void downloadCount(Long id) {
    mapper.downloadCount(id);
  }
}
