package com.stylefeng.guns.modular.qinhe.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.system.service.IDictService;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.SuCategory;
import com.stylefeng.guns.modular.qinhe.service.ISuCategoryService;

/**
 * 模型分类控制器
 *
 * @author fengshuonan
 * @Date 2018-09-07 15:03:49
 */
@Controller
@RequestMapping("/suCategory")
public class SuCategoryController extends BaseController {

  private String PREFIX = "/qinhe/suCategory/";

  @Autowired
  private ISuCategoryService suCategoryService;
  @Autowired
  private IDictService dictService;

  /**
   * 跳转到模型分类首页
   */
  @RequestMapping("")
  public String index() {
    return PREFIX + "suCategory.html";
  }

  /**
   * 跳转到添加模型分类
   */
  @RequestMapping("/suCategory_add")
  public String suCategoryAdd() {
    return PREFIX + "suCategory_add.html";
  }

  /**
   * 跳转到修改模型分类
   */
  @RequestMapping("/suCategory_update/{suCategoryId}")
  public String suCategoryUpdate(@PathVariable Integer suCategoryId, Model model) {
    SuCategory suCategory = suCategoryService.selectById(suCategoryId);
    model.addAttribute("item", suCategory);
    LogObjectHolder.me().set(suCategory);
    return PREFIX + "suCategory_edit.html";
  }

  /**
   * 获取模型分类列表
   */
  @RequestMapping(value = "/list")
  @ResponseBody
  public Object list(String condition) {
    return suCategoryService.selectList(null);
  }

  /**
   * 新增模型分类
   */
  @RequestMapping(value = "/add")
  @ResponseBody
  public Object add(SuCategory suCategory) {
    suCategoryService.insert(suCategory);
    return SUCCESS_TIP;
  }

  /**
   * 删除模型分类
   */
  @RequestMapping(value = "/delete")
  @ResponseBody
  public Object delete(@RequestParam Long suCategoryId) {
    suCategoryService.deleteById(suCategoryId);
    return SUCCESS_TIP;
  }

  /**
   * 修改模型分类
   */
  @RequestMapping(value = "/update")
  @ResponseBody
  public Object update(SuCategory suCategory) {
    suCategoryService.updateById(suCategory);
    return SUCCESS_TIP;
  }

  /**
   * 模型分类详情
   */
  @RequestMapping(value = "/detail/{suCategoryId}")
  @ResponseBody
  public Object detail(@PathVariable("suCategoryId") Integer suCategoryId) {
    return suCategoryService.selectById(suCategoryId);
  }


  @RequestMapping(value = "/tree")
  @ResponseBody
  public List<ZTreeNode> tree() {
    List<ZTreeNode> tree = this.suCategoryService.tree(1);
    tree.add(ZTreeNode.createParent());
    return tree;
  }

  @RequestMapping(value = "/tree2")
  @ResponseBody
  public List<ZTreeNode> tree2() {
    List<ZTreeNode> tree = this.suCategoryService.tree(1);
    for (ZTreeNode zTreeNode : tree) {
      zTreeNode.setOpen(false);
    }
    return tree;
  }

  @RequestMapping(value = "/styles")
  @ResponseBody
  public Object styles() {
    return dictService.selectByParentCode("su_styles");
  }

  @RequestMapping(value = "/matcats")
  @ResponseBody
  public Object materialCategories() {
    return dictService.selectByParentCode("mat_cat");
  }
}
