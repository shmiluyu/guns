package com.stylefeng.guns.modular.qinhe.service;

import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.system.model.SuCategory;
import com.baomidou.mybatisplus.service.IService;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author shmiluyu
 * @since 2018-09-07
 */
public interface ISuCategoryService extends IService<SuCategory> {

  List<ZTreeNode> tree(Integer bigType);
}
