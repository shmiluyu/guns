package com.stylefeng.guns.modular.bean;

import java.io.Serializable;
import org.apache.commons.lang.StringEscapeUtils;

public class Quote implements Serializable {
  private String code;
  private String title;
  private String unit;
  private String price;
  private String remark;

  public static Quote parse(com.stylefeng.guns.modular.system.model.Quote q){
    Quote quote = new Quote();
    quote.setCode(q.getCode());
    quote.setTitle(q.getTitle());
    quote.setUnit(q.getUnit());
    quote.setPrice(q.getPrice());
    quote.setRemark(q.getRemark());
    return quote;
  }
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = StringEscapeUtils.unescapeHtml(title).trim();
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = StringEscapeUtils.unescapeHtml(remark).trim();
  }
}
