package com.stylefeng.guns.modular.qinhe.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.qinhe.model.SuSkmCategory;
import com.stylefeng.guns.modular.qinhe.service.ISuSkmCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 材质分类控制器
 *
 * @author fengshuonan
 * @Date 2018-09-18 12:39:16
 */
@Controller
@RequestMapping("/suSkmCategory")
public class SuSkmCategoryController extends BaseController {

    private String PREFIX = "/qinhe/suSkmCategory/";

    @Autowired
    private ISuSkmCategoryService suSkmCategoryService;

    /**
     * 跳转到材质分类首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "suSkmCategory.html";
    }

    /**
     * 跳转到添加材质分类
     */
    @RequestMapping("/suSkmCategory_add")
    public String suSkmCategoryAdd() {
        return PREFIX + "suSkmCategory_add.html";
    }

    /**
     * 跳转到修改材质分类
     */
    @RequestMapping("/suSkmCategory_update/{suSkmCategoryId}")
    public String suSkmCategoryUpdate(@PathVariable Integer suSkmCategoryId, Model model) {
        SuSkmCategory suSkmCategory = suSkmCategoryService.selectById(suSkmCategoryId);
        model.addAttribute("item",suSkmCategory);
        LogObjectHolder.me().set(suSkmCategory);
        return PREFIX + "suSkmCategory_edit.html";
    }

    /**
     * 获取材质分类列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return suSkmCategoryService.selectList(null);
    }

    /**
     * 新增材质分类
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SuSkmCategory suSkmCategory) {
        suSkmCategoryService.insert(suSkmCategory);
        return SUCCESS_TIP;
    }

    /**
     * 删除材质分类
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer suSkmCategoryId) {
        suSkmCategoryService.deleteById(suSkmCategoryId);
        return SUCCESS_TIP;
    }

    /**
     * 修改材质分类
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SuSkmCategory suSkmCategory) {
        suSkmCategoryService.updateById(suSkmCategory);
        return SUCCESS_TIP;
    }

    /**
     * 材质分类详情
     */
    @RequestMapping(value = "/detail/{suSkmCategoryId}")
    @ResponseBody
    public Object detail(@PathVariable("suSkmCategoryId") Integer suSkmCategoryId) {
        return suSkmCategoryService.selectById(suSkmCategoryId);
    }
}
