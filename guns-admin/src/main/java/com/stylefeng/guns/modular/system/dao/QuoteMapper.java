package com.stylefeng.guns.modular.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.system.model.Quote;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 报价模板 Mapper 接口
 * </p>
 *
 * @author shmiluyu
 * @since 2018-10-15
 */
public interface QuoteMapper extends BaseMapper<Quote> {
  List<com.stylefeng.guns.modular.bean.Quote> json(@Param("pinyin") String pinyin);

}
