package com.stylefeng.guns.modular.qinhe.service;

import com.stylefeng.guns.modular.qinhe.model.SuSkmCategory;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shmiluyu
 * @since 2018-09-18
 */
public interface ISuSkmCategoryService extends IService<SuSkmCategory> {

}
