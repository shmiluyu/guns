package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.SuModel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 模型 Mapper 接口
 * </p>
 *
 * @author shmiuyu
 * @since 2018-09-08
 */
public interface SuModelMapper extends BaseMapper<SuModel> {
  int downloadCount(@Param("modelId") Long id);
}
