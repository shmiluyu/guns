package com.stylefeng.guns.modular.qinhe.controller;

import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.qinhe.service.IQuoteService;
import com.stylefeng.guns.modular.system.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 报价模板控制器
 *
 * @author fengshuonan
 * @Date 2018-10-15 16:22:02
 */
@Controller
@RequestMapping("/quote")
public class QuoteController extends BaseController {

    private String PREFIX = "/qinhe/quote/";

    @Autowired
    private IQuoteService quoteService;

    /**
     * 跳转到报价模板首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "quote.html";
    }

    /**
     * 跳转到添加报价模板
     */
    @RequestMapping("/quote_add")
    public String quoteAdd() {
        return PREFIX + "quote_add.html";
    }

    /**
     * 跳转到修改报价模板
     */
    @RequestMapping("/quote_update/{quoteId}")
    public String quoteUpdate(@PathVariable Long quoteId, Model model) {
        Quote quote = quoteService.selectById(quoteId);
        model.addAttribute("item",quote);
        LogObjectHolder.me().set(quote);
        return PREFIX + "quote_edit.html";
    }

    /**
     * 获取报价模板列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return quoteService.selectList(null);
    }

    /**
     * 新增报价模板
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Quote quote) throws PinyinException {
        quote.setPinyin(PinyinHelper.getShortPinyin(quote.getTitle()));
        quoteService.insert(quote);
        return SUCCESS_TIP;
    }

    /**
     * 删除报价模板
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer quoteId) {
        quoteService.deleteById(quoteId);
        return SUCCESS_TIP;
    }

    /**
     * 修改报价模板
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Quote quote) {
        quoteService.updateById(quote);
        return SUCCESS_TIP;
    }

    /**
     * 报价模板详情
     */
    @RequestMapping(value = "/detail/{quoteId}")
    @ResponseBody
    public Object detail(@PathVariable("quoteId") Integer quoteId) {
        return quoteService.selectById(quoteId);
    }

    @RequestMapping("/json")
    @ResponseBody
    public Object json(@RequestParam(value = "pinyin",required = false) String pinyin){
        return quoteService.json(pinyin);
    }
}
