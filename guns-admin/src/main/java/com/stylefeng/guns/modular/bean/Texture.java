package com.stylefeng.guns.modular.bean;

public class Texture {
  private boolean file;
  private String name;
  private String path;

  public Texture(boolean file, String name, String path) {
    this.file = file;
    this.name = name;
    this.path = path;
  }

  public boolean isFile() {
    return file;
  }

  public void setFile(boolean file) {
    this.file = file;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }
}
