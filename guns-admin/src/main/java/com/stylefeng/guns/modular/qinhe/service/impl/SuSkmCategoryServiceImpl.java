package com.stylefeng.guns.modular.qinhe.service.impl;

import com.stylefeng.guns.modular.qinhe.model.SuSkmCategory;
import com.stylefeng.guns.modular.qinhe.dao.SuSkmCategoryMapper;
import com.stylefeng.guns.modular.qinhe.service.ISuSkmCategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shmiluyu
 * @since 2018-09-18
 */
@Service
public class SuSkmCategoryServiceImpl extends ServiceImpl<SuSkmCategoryMapper, SuSkmCategory> implements ISuSkmCategoryService {

}
