package com.stylefeng.guns.modular.qinhe.dao;

import com.stylefeng.guns.modular.qinhe.model.SuSkmCategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shmiluyu
 * @since 2018-09-18
 */
public interface SuSkmCategoryMapper extends BaseMapper<SuSkmCategory> {

}
