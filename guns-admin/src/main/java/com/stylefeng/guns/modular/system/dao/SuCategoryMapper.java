package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.system.model.SuCategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author shmiluyu
 * @since 2018-09-07
 */
public interface SuCategoryMapper extends BaseMapper<SuCategory> {

  /**
   * 获取ztree的节点列表
   */
  List<ZTreeNode> tree(@Param("bigType") Integer bigType);
}
