package com.stylefeng.guns.modular.qinhe.service;

import com.stylefeng.guns.modular.system.model.SuModel;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 模型 服务类
 * </p>
 *
 * @author shmiuyu
 * @since 2018-09-08
 */
public interface ISuModelService extends IService<SuModel> {
  void downloadCount(Long id);
}
