package com.stylefeng.guns.modular.qinhe.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.qinhe.service.ISuCategoryService;
import com.stylefeng.guns.modular.system.dao.SuCategoryMapper;
import com.stylefeng.guns.modular.system.model.SuCategory;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author shmiluyu
 * @since 2018-09-07
 */
@Service
@Transactional
public class SuCategoryServiceImpl extends ServiceImpl<SuCategoryMapper, SuCategory> implements
    ISuCategoryService {

  @Resource
  private SuCategoryMapper suCategoryMapper;

  @Override
  public List<ZTreeNode> tree(Integer bigType) {
    return this.baseMapper.tree(bigType);
  }
}
