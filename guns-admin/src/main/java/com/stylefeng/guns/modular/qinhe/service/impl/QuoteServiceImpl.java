package com.stylefeng.guns.modular.qinhe.service.impl;

import com.stylefeng.guns.modular.system.model.Quote;
import com.stylefeng.guns.modular.system.dao.QuoteMapper;
import com.stylefeng.guns.modular.qinhe.service.IQuoteService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 报价模板 服务实现类
 * </p>
 *
 * @author shmiluyu
 * @since 2018-10-15
 */
@Service
public class QuoteServiceImpl extends ServiceImpl<QuoteMapper, Quote> implements IQuoteService {

  @Override
  public List<com.stylefeng.guns.modular.bean.Quote> json(String pinyin) {
    return this.baseMapper.json(pinyin);
  }
}
