package com.stylefeng.guns.modular.qinhe.service;

import com.stylefeng.guns.modular.system.model.Quote;
import com.baomidou.mybatisplus.service.IService;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 报价模板 服务类
 * </p>
 *
 * @author shmiluyu
 * @since 2018-10-15
 */
public interface IQuoteService extends IService<Quote> {
  List<com.stylefeng.guns.modular.bean.Quote> json(String pinyin);
}
