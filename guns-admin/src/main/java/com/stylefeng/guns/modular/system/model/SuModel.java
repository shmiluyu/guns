package com.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 模型
 * </p>
 *
 * @author shmiuyu
 * @since 2018-09-08
 */
@TableName("qh_su_model")
public class SuModel extends Model<SuModel> {

  private static final long serialVersionUID = 1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Long id;
  private Long filesize;
  private String modelname;
  private String filename;
  private Long style;
  private Long category;
  private Date createtime;
  private String uploader;

  private Integer vray;
  private Integer enscape;

  private String checksum;
  private Integer downloads;
  private Integer deleted;
  private Integer type = 1;
  private String fileext="skp";
  private String thumbext="png";

  public String getFileext() {
    return fileext;
  }

  public void setFileext(String fileext) {
    this.fileext = fileext;
  }

  public String getThumbext() {
    return thumbext;
  }

  public void setThumbext(String thumbext) {
    this.thumbext = thumbext;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getChecksum() {
    return checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public Integer getDownloads() {
    return downloads;
  }

  public void setDownloads(Integer downloads) {
    this.downloads = downloads;
  }

  public Integer getVray() {
    return vray;
  }

  public void setVray(Integer vray) {
    this.vray = vray;
  }

  public Integer getEnscape() {
    return enscape;
  }

  public void setEnscape(Integer enscape) {
    this.enscape = enscape;
  }

  public Integer getThea() {
    return thea;
  }

  public void setThea(Integer thea) {
    this.thea = thea;
  }

  private Integer thea;

  public Integer getDeleted() {
    return deleted;
  }

  public void setDeleted(Integer deleted) {
    this.deleted = deleted;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getFilesize() {
    return filesize;
  }

  public void setFilesize(Long filesize) {
    this.filesize = filesize;
  }

  public String getModelname() {
    return modelname;
  }

  public void setModelname(String modelname) {
    this.modelname = modelname;
  }

  public Long getStyle() {
    return style;
  }

  public void setStyle(Long style) {
    this.style = style;
  }

  public Long getCategory() {
    return category;
  }

  public void setCategory(Long category) {
    this.category = category;
  }

  public Date getCreatetime() {
    return createtime;
  }

  public void setCreatetime(Date createtime) {
    this.createtime = createtime;
  }

  public String getUploader() {
    return uploader;
  }

  public void setUploader(String uploader) {
    this.uploader = uploader;
  }

  @Override
  protected Serializable pkVal() {
    return this.id;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  @Override
  public String toString() {
    return "SuModel{" +
        "id=" + id +
        ", filesize=" + filesize +
        ", modelname=" + modelname +
        ", style=" + style +
        ", category=" + category +
        ", createtime=" + createtime +
        ", uploader=" + uploader +
        "}";
  }
}
