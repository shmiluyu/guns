package com.stylefeng.guns;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * SpringBoot方式启动类
 *
 * @author stylefeng
 * @Date 2017/5/21 12:06
 */
@SpringBootApplication
@EnableAsync
public class GunsApplication {

  private final static Logger logger = LoggerFactory.getLogger(GunsApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(GunsApplication.class, args);
    new File(new ApplicationHome(GunsApplication.class).getDir()+"/skm/textures").mkdirs();
    new File(new ApplicationHome(GunsApplication.class).getDir()+"/skm/thumbs").mkdirs();
    logger.info("GunsApplication is success!");
  }
}
