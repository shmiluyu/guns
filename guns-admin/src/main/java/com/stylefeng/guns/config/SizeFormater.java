package com.stylefeng.guns.config;

import org.beetl.core.Format;

public class SizeFormater implements Format {

  @Override
  public Object format(Object data, String s) {
    if (data == null) {
      return null;
    }
    if (Long.class.isAssignableFrom(data.getClass())) {
      long size = Long.parseLong(data.toString());
      if (size > 1048576) {
        return Math.floorDiv(size, (1048576)) + "MB";
      } else {
        return Math.floorDiv(size, 1024) + "Kb";
      }
    } else {
      throw new RuntimeException("Arg Error:Type should be Date");
    }
  }
}
